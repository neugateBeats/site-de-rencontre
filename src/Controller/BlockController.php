<?php

namespace App\Controller;

use App\Entity\Block;
use App\Entity\User;
use App\Repository\BlockRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/block")
*/
class BlockController extends AbstractController
{

    /**
     * @Route("/block-{username}", name="block_blocked")
     */
    public function displayBlocked(string $username, BlockRepository $blockRepository, UserRepository $userRepository): Response
    {
        $block = new Block();
       
        
        $blocked = $userRepository->findOneBy(['username' => $username]);
        
        $user =$this->getUser();
        $block->setBlocked($blocked);
        $block->setBlocker($user);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($block);
        $entityManager->flush();

        return $this->redirectToRoute("block_view");

        return $this->render('block/blocked.html.twig', [
            'users' => $user,
            'blocks' => $block,

        ]);
    
    }
    
    /**
     * @Route("/view", name="block_view")
     */
    function viewBlocked(BlockRepository $blockRepository, UserRepository $userRepository): Response
    {
        $blockRepository = $this->getDoctrine()->getRepository(Block::class);       
        $blocked = $blockRepository->findAll();
        $users = $userRepository->findAll();
        $usersToDisplay = [];
        $userConnected = $this->getUser();


        foreach ($users as $user) {
            if ($user != $userConnected) {
                foreach ($blocked as $b){
                    if ($b->getBlocker() == $userConnected and $b->getBlocked() == $user){
                        array_push($usersToDisplay, $user);
                        break;
                    }
                }
            }
        }
         

        return $this->render('block/view.html.twig', [
            "users" => $usersToDisplay,
            "blocked" => $blocked
            ]);
    }


        
    
    


}    