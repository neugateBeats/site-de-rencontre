<?php
namespace App\Controller;


use App\Entity\Messagerie;
use App\Repository\MessagerieRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/messagerie")
 */

class MessagerieController extends AbstractController
{
    /**
     * @Route("/messagerie/index", name="messagerie_index", methods={"GET"} )
     */

    public function index(MessagerieRepository $messagerieRepository): Response
    {

        return $this->render('messagerie/index.html.twig',
            [
                "messageries" => $messagerieRepository->findAll(),
            ]
        );
    }

     /**
     * @Route("/messagerie/sent/", name="messagerie_sent", methods={"GET"})
     */
    public function sent(MessagerieRepository $messagerieRepository): Response
    {
        
        return $this->render('messagerie/sent.html.twig',
            ["messageries" => $messagerieRepository->findAll(),
        ]); 
    }


    /**
     * @Route("/create/{username}", name="messagerie_create", methods= {"GET", "POST"})
     */
    public function create(string $username, Request $request, UserRepository $userRepository): Response
    {
    
        $messagerie = new Messagerie();
        $form = $this->createFormBuilder($messagerie)
            ->add('content')
            ->add('submit', SubmitType::class, ['label' => 'Create Messagerie'])
            ->getForm();
        $form->handleRequest($request);
        $user = $userRepository->findOneBy(["username" => $username]);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $user =$this->getUser();
            $messagerie->setAuthor($user);
            $messagerie->setCreatedAt(new \DateTime());

            $receiver = $userRepository->findOneBy(["username" => $username]);
            $messagerie->setReceiver($receiver);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($messagerie);
            $entityManager->flush();

            return $this->redirectToRoute("messagerie_sent"); 

        }  

        return  $this->render('messagerie/create.html.twig',
        [
            'messagerie' => $messagerie,
            'form' => $form->createView(),
            "user" => $user
        ]);
        
    }
    
   

    /**
     * @Route("/messagerie/delete/{id}", name="messagerie_delete")
     */
    public function delete(Request $request, Messagerie $messagerie): Response
    {
        

       
        $messagerie = $this->getDoctrine()->getRepository(Messagerie::class)->find($messagerie);

        
        if (!$messagerie) {
           
            throw $this->createNotFoundException("Messagerie does not exist");
        }

        
        if ($messagerie->getAuthor() != $this->getUser()) {
            throw $this->createAccessDeniedException();
        }

       
        $em = $this->getDoctrine()->getManager();
       
        $em->remove($messagerie);
        $em->flush();

    
        return $this->redirectToRoute('home_index');
    }

}